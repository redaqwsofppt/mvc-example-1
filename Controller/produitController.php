<?php
require_once "./Repository/irepoProduit.php";
require_once "./Repository/repoProduit.php";

class ProduitController {

	private IRepoProduit $repo_produit;

	public function __construct(IRepoProduit $repo){
		$this->repo_produit = $repo;
	}
	
	public function index(){
		$data = $this->repo_produit->get_tous_produits();
		require_once "./View/index_produit.php";
	}

	public function create() {
		require_once "./View/creer_produit.php";
	}

	public function store() {
		$id = $_POST["id"];
		$designation = $_POST["designation"];
		$prix_unitaire = $_POST["prix_unitaire"];

		$produit = new Produit(intval($id), $designation, floatval($prix_unitaire));
		$this->repo_produit->ajouter_produit($produit);
		header("Location:/produit");
		// require_once "./View/creer_produit.php";
	}

	public function show(int $id) {
		$produit = $this->repo_produit->rechercher_produit($id);
		if ($produit != null) {
			require_once "./View/modifier_produit.php";
		}
	}

	public function update(int $id) {
		$designation = $_POST["designation"];
		$prix_unitaire = $_POST["prix_unitaire"];

		$produit = new Produit(intval($id), $designation, floatval($prix_unitaire));
		$this->repo_produit->modifier_produit($id, $produit);
		header("Location:/produit");
		// require_once "./View/creer_produit.php";
	}

	public function destroy(int $id) {
		
		$this->repo_produit->supprimer_produit($id);
		header("Location:/produit");

	}

	
}