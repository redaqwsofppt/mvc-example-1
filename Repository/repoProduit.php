
<?php
require_once "irepoProduit.php";
require_once "connection.php";
require_once "utilities/utility.php";


class RepoProduit implements IRepoProduit {
	private PDO $pdoObject;

	public function __construct(){
		$this->pdoObject = Connection::connect_to_database();
	}

	public function ajouter_produit(Produit $produit): void{
		$stmt = $this->pdoObject->prepare("INSERT INTO produit (id, designation, prix_unitaire) VALUES (:id, :designation, :prix_unitaire);");

		$id = Utility::clean_input(strval($produit->get_id()));
		$designation = Utility::clean_input($produit->get_designation());
		$prix_unitaire = Utility::clean_input(strval($produit->get_prix_unitaire()));

		$stmt->execute([
			":id" => intval($id),
			":designation" => $designation,
			":prix_unitaire" => floatval($prix_unitaire)
		]);
	}
	
	public function modifier_produit(int $id, Produit $produit): void{
		$stmt = $this->pdoObject->prepare("UPDATE produit
		SET designation = :designation, prix_unitaire = :prix_unitaire
		WHERE id = :id;");

		$id = Utility::clean_input(strval($id));
		$designation = Utility::clean_input($produit->get_designation());
		$prix_unitaire = Utility::clean_input(strval($produit->get_prix_unitaire()));

		$stmt->execute([
			":designation" => $designation,
			":prix_unitaire" => floatval($prix_unitaire),
			":id" => intval($id)
		]);
	}

	public function supprimer_produit(int $id): void{
		$stmt = $this->pdoObject->prepare("DELETE FROM produit
		WHERE id = :id");

		$id = Utility::clean_input(strval($id));

		$stmt->execute([
			":id" => $id
		]);
	}

	public function rechercher_produit(int $id): Produit{
		$stmt = $this->pdoObject->prepare("SELECT * FROM produit WHERE id = :id;");

		$id = Utility::clean_input(strval($id));
		$stmt->bindParam(":id", $id);
		$stmt->execute();

		$fetchedObj = $stmt->fetch(PDO::FETCH_ASSOC);

		return new Produit(intval($fetchedObj["id"]), $fetchedObj["designation"], floatval($fetchedObj["prix_unitaire"]));
	}

	public function get_tous_produits(): array{
		$stmt = $this->pdoObject->prepare("SELECT * FROM produit;");
		$stmt->execute();

		$fetchedData = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return $fetchedData;
	}
}