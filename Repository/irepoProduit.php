<?php

interface IRepoProduit {
	public function ajouter_produit(Produit $produit): void;
	public function modifier_produit(int $id, Produit $produit): void;
	public function supprimer_produit(int $id): void;
	public function rechercher_produit(int $id): Produit;
	public function get_tous_produits(): array;
}
