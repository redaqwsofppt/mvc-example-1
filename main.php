<?php

require_once "Repository/repoProduit.php";
require_once "Model/produit.php";
require_once "Controller/produitController.php";

$prd1 = new  Produit(1, "nutella", 14521);
$prd2 = new  Produit(2, "colgate", 156);
$prd3 = new  Produit(3, "signal", 421);
$prd4 = new  Produit(4, "DELL", 156);
$prd5 = new  Produit(5, "HP", 156);
$prd6 = new  Produit(6, "ASUS", 156);
$prd7 = new  Produit(7, "ROG", 156);
$repo = new RepoProduit();
// $prd_controller = new ProduitController();

// $repo->ajouter_produit($prd2);
// $repo->ajouter_produit($prd4);
// $repo->ajouter_produit($prd5);
// $repo->ajouter_produit($prd6);
// $repo->ajouter_produit($prd7);
// $repo->ajouter_produit($prd3);
// $repo->modifier_produit(2, $prd3);
// $repo->supprimer_produit(2);
// echo $repo->rechercher_produit(1);
// print_r($prd_controller->index());

// foreach ($prd_controller->index() as $key => $value) {
// 	echo $value["id"];
// 	echo $value["designation"];
// 	echo $value["prix_unitaire"];
// }

$prd_controller = new ProduitController($repo);

$prd_controller->index();
