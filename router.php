<?php
require_once "Repository/repoProduit.php";
require_once "Model/produit.php";
require_once "Controller/produitController.php";


$request_m = $_SERVER["REQUEST_METHOD"];
$uri = $_SERVER["REQUEST_URI"];
// echo $request_u;

if (preg_match("{/\w+/(\d+)}" , $uri, $match)) {
	// print_r($match);
	$id = $match[1];
	// echo $id;
} 

switch ($request_m) {
	case 'GET':

		switch ($uri) {
			
			case '/':
				// echo "index";
				// require_once __DIR__  . "/index.php";
				$repo = new RepoProduit();
                $prd_controller = new ProduitController($repo);
                $prd_controller->index();
                
				break;
			case '/produit':
				$repo = new RepoProduit();
                $prd_controller = new ProduitController($repo);
                $prd_controller->index();

				break;
			case '/produit/create':
				$repo = new RepoProduit();
                $prd_controller = new ProduitController($repo);
                $prd_controller->create();
				break;
			case "/produit/$id":
				$repo = new RepoProduit();
				$prd_controller = new ProduitController($repo);
				// $prd_controller->show($id);
				
				break;
			case "/produit/$id/edit":
				$repo = new RepoProduit();
				$prd_controller = new ProduitController($repo);
				$prd_controller->show($id);
				break;
		}
		break;

	case 'POST':
		if ($uri == "/produit") {
			$repo = new RepoProduit();
			$prd_controller = new ProduitController($repo);
			$prd_controller->store();
		} else if ($uri == "/produit/$id") {
			$repo = new RepoProduit();
			$prd_controller = new ProduitController($repo);
			$prd_controller->update($id);
		} 
		break;
		
	case 'PUT':
		if ($uri == "/produit/$id") {
			echo "put produit ";
		} else {
			echo "error: 404 page not found";
		}
		break;
			
	case 'DELETE':
		if ($uri == "/produit/$id") {
			$repo = new RepoProduit();
			$prd_controller = new ProduitController($repo);
			$prd_controller->destroy($id);
	} else {
		echo "error: 404 page not found";
	}
	break;
	
	default:
		echo "error 404: page not found";
		break;
}
